#include "funciones.h"
#include <iostream>

using namespace std;

int main () {
	int choice, a, n, c;
	cout << "Digite una opcion:\n1: Potencia\n2: Busqueda exhaustiva\n3: Ordenamiento por burbuja\n4: Ordenamiento por seleccion\n5: Emparejamiento de parejas" << endl;
	cin >> choice;

	switch ( choice ) {
		case 1: {
			int base, exponent, result;
			cout << "Ingrese un numero para elevar:" << endl;
			cin >> base;
			cout << "Ingrese un numero potencia:" << endl;
			cin >> exponent;
			result = plot ( base, exponent );
			cout << result << endl;
			break;
		}
		case 2: {
			int arr[] = { 1, 2, 3, 4, 5, 6 };
			int item = 0;
			int found = busquedaSec ( arr, 6, item );
			cout << "Elemento " << item << ", encontrado en posicion "<< found << endl;
			break;
		}
		case 3: {
			int arrN[] = { 0, 6 , 7, 5, 1, 50};
			ordenamientoBurbuja(arrN, 6 );
			for ( int i = 0; i < 6; i++ ) {
				cout << arrN[i] << endl;
			}
			break;
		}
		case 4: {
			int arrS[] = { 10, 6 , 2, 7, 5, 1, 50};
			ordenamientoSeleccion ( arrS, 7 );
			for (int i = 0; i < 7; i++) {
				cout << arrS[i] << endl;
			}
			break;
		}
		case 5: {
			char word[] = {'C', 'o', 'g', 'i', 't', 'o', 'e', 'r', 'g', 'o', 's', 'u', 'm'};
			char found[] = {'e', 'r', 'g', 'o'};
			int result = emparejamientoCadenas ( word, 13, found, 4 );
			cout << "Encontrado en la posicion: " << result << endl;
			break;
		}
	}
}

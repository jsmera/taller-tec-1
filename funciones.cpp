int plot ( int a, int n ) {
	int i, result; // 2
	result = 1; // 1
	for ( i = 1; i <= n; i++ ) { // n
		result *= a; // n
	}
	return result; // 1
}

int busquedaSec ( int arreglo[], int n, int k ) {
	int i; // 1
	for( i = 0; i < n; i++ ) { // 1 (n/2)+1  n+1
		if ( arreglo[i] == k ) { // 1 n/2 n
			return i; // 1
		}
	}
	return -1; // 1
}

void ordenamientoBurbuja ( int *A, int n ) {
	for ( int i = 0; i < n - 2; i++ ) {// best medium bad: n-1
		for ( int j = 0; j < n - 2 - i; j++ ) {// best medium bad: (n-1)(n)/2 
			if ( A[j + 1] < A[j] ) { // best medium bad: (n-1)(n)/2
				int temp = A[j + 1]; // medium: (n-1)(n)/4
				A[j + 1] = A[j]; // medium: (n-1)(n)/4
				A[j] = temp; // medium: (n-1)(n)/4
			}
		}
	}
}

void ordenamientoSeleccion ( int *A, int n ) {
	for ( int i = 0; i < n-2; i++ ) { // best: n-1
		int min = i; // 1
		for ( int j = i + 1; j < n - 1; j++ ) { // (n-1)(n)/2
			if ( A[j] < A[min] ) {
				min = j;
			}
		}
		int temp = A[i];
		A[i] = A[min];
		A[min] = temp;
	}
}

int emparejamientoCadenas ( char T[], int n, char P[], int m ) {
	for ( int i =0; i < n - m; i++ ) {
		int j = 0;
		while ( (j < m) && (P[j] == T[i + j]) ) {
			j += 1;
		}
		if (j == m) {
			return i;
		}
	}
	return -1;
}